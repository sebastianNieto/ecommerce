<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

  public function createProduct($data){
    $this->db->insert('tblproducto', $data);
    return $this->db->affected_rows();
  }
}