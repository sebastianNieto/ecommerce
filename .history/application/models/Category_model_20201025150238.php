<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

  public function getCategories(){
    $sql = $this->db->get('tblcategory');
    return $sql->result();
  }
}