<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Login extends RestController {
  public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function login_get()
    {
        $user = $this->get( 'user' );
        $password = $this->get( 'password' );
        $this->response( ['user' => $user], 200 );
    }
}
