<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Product extends RestController {
  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, OPTIONS");
    $this->load->library('form_validation');
    $this->load->library("JWT"); 
    $this->load->model('Product_model'); 
  }

  public function getProducts_get() {

  }

  public function createProduct_post() {
    $output = ['status' => 0, 'data' => 'No se pudo crear el producto'];
    $nombre = $this->post( 'nombre' );
    $descripcion = $this->post( 'descripcion' );
    $precio = $this->post( 'precio' );
    $categoria = $this->post( 'categoria' );
    $image = $this->post( 'image' );
    $nameImage = $nombre.date('Y-m-d');
    $base64= base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
    $nombre_imagen = $base64['name'];
    $this->response( $nombre_imagen , 200 );
  }
}