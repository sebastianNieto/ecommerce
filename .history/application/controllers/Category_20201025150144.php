<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Category extends RestController {
  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, OPTIONS");
    $this->load->library('form_validation');
    $this->load->library("JWT"); 
    $this->load->model('Category_model'); 
  }

  public function getCategories_get() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->get('token'), '3c0MM3rc3');
    if($token) {
      $categories = $this->db->Category_model->getCategories();
      $output = ['status' => 1, 'data' => $categories];
    }
    $this->response( $output, 200 );
  }

}