<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Login extends RestController {
  public function __construct()
    {
      parent::__construct();
      header('Access-Control-Allow-Origin: *');
      header("Access-Control-Allow-Methods: GET, OPTIONS");
      $this->load->library('form_validation');
      $this->load->library("JWT"); 
      $this->load->model('Login_model'); 
    }

    public function login_post()
    {
      $output = ['status' => 0, 'data' => 'Usuario inactivo o inexistente'];
      $user = $this->post( 'user' );
      $password = $this->post( 'password' );
      $userDatabase = $this->Login_model->validate_user($user);
      if($userDatabase) {
        $output = ['status' => 0, 'data' => 'Erro al autenticar'];
        $passwordDatabase = $userDatabase->password;
        if(password_verify($password, $passwordDatabase)) {
          $data['dataUser'] = ['user' => $userDatabase->user, 'name' => $userDatabase->name];
          $date = new DateTime();
          $data['iat'] = $date->getTimestamp();
          $data['exp'] = $date->getTimestamp() + 1200;
          $data['token'] = $this->jwt->encode($data, "3c0MM3rc3");
          $output = ['status' => 1, 'data' => $data];
        }

      }
      $this->response( $output, 200 );
    }
}
