<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Product extends RestController {
  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, OPTIONS");
    $this->load->library('form_validation');
    $this->load->library("JWT"); 
    $this->load->model('Product_model'); 
  }

  public function getProducts_get() {

  }

  public function createProduct_post() {
    $output = ['status' => 0, 'data' => 'No se pudo crear el producto'];
    $nombre = $this->post( 'nombre' );
    $descripcion = $this->post( 'descripcion' );
    $precio = $this->post( 'precio' );
    $categoria = $this->post( 'categoria' );
    $image = $this->post( 'image' );
    $imagebase64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
    $img = explode(',', $image);
    $ini =substr($img[0], 11);
    $type = explode(';', $ini);
    $rutaImagenSalida = "uploads/products/" . $nameImage = str_replace(' ', '', $nombre).date('Y-m-d').'.'.$type[0];
    $bytes = file_put_contents($rutaImagenSalida, $imagebase64);
    $this->response( $type, 200 );
  }
}