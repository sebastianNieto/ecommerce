<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Login extends RestController {
  public function __construct()
    {
      parent::__construct();
      $this->load->library('form_validation');
      $this->load->model('Login_model');
    }

    public function login_get()
    {
      $output = ['status' => 0, 'No existe el usuario en la base de datos'];
      $user = $this->get( 'user' );
      $password = $this->get( 'password' );
      $userDatabase = $this->Login_model->validate_user($user);
      if($userDatabase) {
        $output = ['status' => 0, 'Erro al autenticar'];
        $passwordDatabase = $userDatabase->password;
        if(password_verify($password, $passwordDatabase)) {
          $output = ['status' => 1, 'Login exitoso'];
        }

      }
      $this->response( $output, 200 );
    }
}
