<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Product extends RestController {
  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Methods: GET, OPTIONS");
    $this->load->library('form_validation');
    $this->load->library("JWT"); 
    $this->load->model('Product_model'); 
  }

  public function getProducts_get() {

  }

  public function createProduct() {
    $output = ['status' => 0, 'data' => 'No se pudo crear el producto'];
    $user = $this->post( 'user' );
    $password = $this->post( 'password' );
  }
}