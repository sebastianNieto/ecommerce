<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Login extends RestController {
  public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Login');
    }

    public function login_get()
    {
        $user = $this->get( 'user' );
        $password = $this->get( 'password' );
        $this->response( ['user' => $user], 200 );
    }
}
