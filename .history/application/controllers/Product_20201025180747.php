<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Product extends RestController {
  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    header("Allow: GET, POST, OPTIONS, PUT, DELETE");
    $this->load->library('form_validation');
    $this->load->library("JWT"); 
    $this->load->model('Product_model'); 
  }

  public function getProducts_get() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->get('token'), '3c0MM3rc3');
    if($token) {
      $products = $this->Product_model->getProducts();
      $output = ['status' => 1, 'data' => $products];
    }
    $this->response( $output, 200 );
  }

  public function createProduct_post() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->post('token'), '3c0MM3rc3');
    if($token) {
      $output = ['status' => 0, 'data' => 'No se pudo crear el producto'];
      $nombre = $this->post( 'nombre' );
      $descripcion = $this->post( 'descripcion' );
      $precio = $this->post( 'precio' );
      $categoria = $this->post( 'categoria' );
      $image = $this->post( 'image' );
      $imagebase64 = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
      $img = explode(',', $image);
      $ini =substr($img[0], 11);
      $type = explode(';', $ini);
      $nameImage = str_replace(' ', '', $nombre).date('Y-m-d').'.'.$type[0];
      $rutaImagenSalida = "uploads/products/" . $nameImage;
      $bytes = file_put_contents($rutaImagenSalida, $imagebase64);
      if($bytes) {
        $data = [
          'name' => $nombre,
          'description' => $descripcion,
          'price' => $precio,
          'image' => $nameImage,
          'category' => $categoria
        ];

        $register = $this->Product_model->createProduct($data);

        if($register) {
          $output = ['status' => 1, 'data' => 'Registro creado exitosamente'];
        }
      }
    }
    $this->response( $output, 200 );
  }

  public function deleteProduct_delete() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->get('token'), '3c0MM3rc3');
    if($token) {
      $id = $this->delete( 'id' );
      $register = $this->Product_model->deleteProduct($id);
      $output = ['status' => 1, 'data' => 'No se eliminó el registro'];
      if($register) {
          $output = ['status' => 1, 'data' => $products];
      }
      $output = ['status' => 1, 'data' => $products];
    }
    $this->response( $output, 200 );
  }
}