<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Login extends RestController {
  public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Login_model');
    }

    public function login_get()
    {
        $user = $this->get( 'user' );
        $password = $this->get( 'password' );
        $userDatabase = $this->Login_model->validate_user($user);
        $this->response( ['user' => $userDatabase], 200 );
    }
}
