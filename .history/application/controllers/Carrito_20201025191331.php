<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Product extends RestController {
  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
    header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization');
    $this->load->library('form_validation');
    $this->load->library("JWT"); 
    $this->load->model('Carrito_model'); 
  }

  public function ingresar_post() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->post('token'), '3c0MM3rc3');
    if($token) {
      $idProduct = $this->post('id');
      $cantidad = $this->post('cantidad');
      $user = $token['user'];
    }
    $this->response( $user, 200 );
  }

  public function getCart_get() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->get('token'), '3c0MM3rc3');
    if($token) {
      $id = $this->get( 'id' );
      $output = ['status' => 1, 'data' => 'No existe el producto'];
      $product = $this->Product_model->getProduct($id);
      if($product) {
        $output = ['status' => 1, 'data' => $product];
      }
    }
    $this->response( $output, 200 );
  }
}