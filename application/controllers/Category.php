<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Category extends RestController {
  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
    header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization');
    $this->load->library('form_validation');
    $this->load->library("JWT"); 
    $this->load->model('Category_model'); 
  }

  public function getCategories_get() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->get('token'), '3c0MM3rc3');
    if($token) {
      $categories = $this->Category_model->getCategories();
      $output = ['status' => 1, 'data' => $categories];
    }
    $this->response( $output, 200 );
  }

  public function createCategory_post() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->post('token'), '3c0MM3rc3');
    if($token) {
      $output = ['status' => 0, 'data' => 'No se pudo crear la categoría'];
      $nombre = $this->post( 'nombre' );
      $descripcion = $this->post( 'descripcion' );
      $data = [
        'name' => strtoupper($nombre),
        'description' => $descripcion,
      ];
      $register = $this->Category_model->createCategory($data);

      if($register) {
        $output = ['status' => 1, 'data' => 'Registro creado exitosamente'];
      }
    }
    $this->response( $output, 200 );
  }

  public function deleteCategory_post() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->post('token'), '3c0MM3rc3');
    if($token) {
      $id = $this->post( 'id' );
      $register = $this->Category_model->deleteCategory($id);
      $output = ['status' => 0, 'data' => 'No se eliminó el registro'];
      if($register) {
          $output = ['status' => 1, 'data' => 'Registro eliminado correctamente'];
      }
    }
    $this->response( $output, 200 );
  }

}