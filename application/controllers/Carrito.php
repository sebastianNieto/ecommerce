<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Carrito extends RestController {
  public function __construct()
  {
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
    header('Access-Control-Allow-Headers: Content-Type, X-Auth-Token, Origin, Authorization');
    $this->load->library('form_validation');
    $this->load->library("JWT"); 
    $this->load->model('Carrito_model'); 
  }

  public function getCarrito_get() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->get('token'), '3c0MM3rc3');
    if($token) {
      $carrito = $this->Carrito_model->getCarrito($token->dataUser->user);
      $output = ['status' => 1, 'data' => $carrito];
    }
    $this->response( $output, 200 );
  }

  public function ingresar_post() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->post('token'), '3c0MM3rc3');
    if($token) {
      $data['id_producto'] = $this->post('product');
      $data['cantidad'] = $this->post('cant');
      $data['user'] = $token->dataUser->user;
      $dataCarrito = $this->Carrito_model->getCarrito($data['user'], $data['id_producto']);
      $output = ['status' => 0, 'data' => 'No se pudo registrar el item'];
      if($dataCarrito) {
        $registro = $this->Carrito_model->updateCarrito($data['user'], $data['id_producto'], $data['cantidad'] + $dataCarrito[0]->cantidad);
      }
      else {
        $registro = $this->Carrito_model->ingresar($data);
      }
      if($registro) {
        $output = ['status' => 1, 'data' => 'Item agregado con éxito'];
      }
    }
    $this->response( $output, 200 );
  }

  public function deleteCarrito_post() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->post('token'), '3c0MM3rc3');
    if($token) {
      $id = $this->post( 'id' );
      $register = $this->Carrito_model->deleteCarrito($id);
      $output = ['status' => 0, 'data' => 'No se eliminó el registro'];
      if($register) {
          $output = ['status' => 1, 'data' => 'Registro eliminado correctamente'];
      }
    }
    $this->response( $output, 200 );
  }

  public function updateCarrito_post() {
    $output = ['status' => -1, 'data' => 'Token Invalido'];
    $token = $this->jwt->decode($this->post('token'), '3c0MM3rc3');
    if($token) {
      $id = $this->post( 'producto' );
      $cantidad = $this->post( 'cantidad' );
      $output = ['status' => 0, 'data' => 'No se actualizó el registro'];
      $register = $this->Carrito_model->updateCarrito($token->dataUser->user, $id, $cantidad);
      
      if($register) {
          $output = ['status' => 1, 'data' => 'Registro actualizado correctamente'];
      }
    }
    $this->response( $output, 200 );
  }
}