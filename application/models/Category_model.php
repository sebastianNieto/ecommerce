<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

  public function getCategories(){
    $sql = $this->db->get('tblcategory');
    return $sql->result();
  }

  public function createCategory($data){
    $this->db->insert('tblcategory', $data);
    return $this->db->affected_rows();
  }

  public function deleteCategory($id) {
    $this->db->where('id', $id);
    $this->db->delete('tblcategory');
    return $this->db->affected_rows();
  }
}