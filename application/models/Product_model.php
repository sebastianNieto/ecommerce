<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

  public function createProduct($data){
    $this->db->insert('tblproducto', $data);
    return $this->db->affected_rows();
  }

  public function getProducts($param, $category) {
    if(!empty($param)){
      $this->db->like('upper(description)', strtoupper($param));
      $this->db->or_like('upper(name)', strtoupper($param));
      $this->db->or_like('upper(price)', strtoupper($param));
    }
    if(!empty($category)){
      $this->db->where('category', $category);
    }
    $sql = $this->db->get('tblproducto');
    return $sql->result();
  }

  public function getProduct($id) {
    $this->db->where('id', $id);
    $sql = $this->db->get('tblproducto');
    return $sql->row();
  }

  public function deleteProduct($id) {
    $this->db->where('id', $id);
    $this->db->delete('tblproducto');
    return $this->db->affected_rows();
  }
}