<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carrito_model extends CI_Model {

  public function getCarrito($user, $product=''){
    if(!empty($product)) {
      $this->db->where('id_producto', $product);
    }
    $this->db->where('user', $user);
    $this->db->join('tblproducto', 'tblcarrito.id_producto = tblproducto.id');
    $this->db->select('tblcarrito.id as id, tblproducto.description as producto, tblcarrito.cantidad, tblproducto.price as precio, (tblproducto.price * tblcarrito.cantidad) as subtotal');
    $sql = $this->db->get('tblcarrito');
    return $sql->result();
  }

  public function ingresar($data){
    $this->db->insert('tblcarrito', $data);
    return $this->db->affected_rows();
  }

  public function updateCarrito($user, $product, $cantidad) {
    $this->db->where('user', $user);
    $this->db->where('id_producto', $product);
    $this->db->set("cantidad", $cantidad);
    $sql = $this->db->update('tblcarrito');
    return $this->db->affected_rows();
  }

  public function deleteCarrito($id) {
    $this->db->where('id', $id);
    $this->db->delete('tblcarrito');
    return $this->db->affected_rows();
  }
}