-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-11-2020 a las 22:53:49
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ecommerce`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcarrito`
--

CREATE TABLE `tblcarrito` (
  `id` smallint(6) NOT NULL,
  `id_producto` smallint(6) NOT NULL,
  `user` varchar(20) NOT NULL,
  `cantidad` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblcarrito`
--

INSERT INTO `tblcarrito` (`id`, `id_producto`, `user`, `cantidad`) VALUES
(1, 1, 'ADMIN', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblcategory`
--

CREATE TABLE `tblcategory` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblcategory`
--

INSERT INTO `tblcategory` (`id`, `name`, `description`, `date_creation`) VALUES
(1, 'GENERAL', 'Productos de una categoría general', '2020-10-25 14:51:30'),
(2, 'NUEVOS', 'Nuevos productos', '2020-10-25 14:51:48'),
(3, 'PLUS', 'Productos plus de la empresa', '2020-10-25 14:52:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblproducto`
--

CREATE TABLE `tblproducto` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  `price` int(4) NOT NULL,
  `image` varchar(100) NOT NULL,
  `category` smallint(6) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tblproducto`
--

INSERT INTO `tblproducto` (`id`, `name`, `description`, `price`, `image`, `category`, `date_creation`) VALUES
(1, 'Chaqueta', 'Chaqueta para un perrito', 50000, 'Chaqueta2020-10-25.png', 1, '2020-10-25 16:15:49'),
(2, 'Nueva Chaqueta', 'Chaqueta para un perrito', 50000, 'NuevaChaqueta2020-10-25.png', 2, '2020-10-25 16:17:30'),
(6, 'Chaqueta Roja', 'Esta es una chaqueta roja', 5000000, 'ChaquetaRoja2020-10-25.jpeg', 2, '2020-10-25 17:39:43'),
(9, 'Chaqueta Azul', 'Esta es una chaqueta azul', 2000000, 'ChaquetaAzul2020-10-25.jpeg', 2, '2020-10-25 17:42:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbluser`
--

CREATE TABLE `tbluser` (
  `id` smallint(4) NOT NULL,
  `user` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` enum('ACT','INA') NOT NULL,
  `date_register` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbluser`
--

INSERT INTO `tbluser` (`id`, `user`, `name`, `password`, `status`, `date_register`) VALUES
(1, 'ADMIN', 'Administrador del sistema', '$2y$10$nkiN/hu.Hrc.CwmuaVu2Ie0GpXOEGIxTgSw12Gm6V5nD78WPniNNy', 'ACT', '2020-10-25 10:12:18');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblcarrito`
--
ALTER TABLE `tblcarrito`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_producto` (`id_producto`),
  ADD KEY `user` (`user`),
  ADD KEY `user_2` (`user`);

--
-- Indices de la tabla `tblcategory`
--
ALTER TABLE `tblcategory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `tblproducto`
--
ALTER TABLE `tblproducto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`);

--
-- Indices de la tabla `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`(10)),
  ADD KEY `user_2` (`user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblcarrito`
--
ALTER TABLE `tblcarrito`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tblcategory`
--
ALTER TABLE `tblcategory`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tblproducto`
--
ALTER TABLE `tblproducto`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `tbluser`
--
ALTER TABLE `tbluser`
  MODIFY `id` smallint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tblcarrito`
--
ALTER TABLE `tblcarrito`
  ADD CONSTRAINT `tblcarrito_ibfk_1` FOREIGN KEY (`id_producto`) REFERENCES `tblproducto` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tblcarrito_ibfk_2` FOREIGN KEY (`user`) REFERENCES `tbluser` (`user`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tblproducto`
--
ALTER TABLE `tblproducto`
  ADD CONSTRAINT `tblproducto_ibfk_1` FOREIGN KEY (`category`) REFERENCES `tblcategory` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
